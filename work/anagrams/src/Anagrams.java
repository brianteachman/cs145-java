import java.util.*;

/**
 * An anagram is a word or phrase made by rearranging the letters of another
 * word or phrase. This class uses recursive backtracking to find all the
 * anagram words it can.
 *
 * @author Brian Teachman
 */
public class Anagrams {

    private Set<String> dictionary;
    private Set<String> words;
    private HashMap<String, Set<String>> wordsMap; // phrase, wordList

    /**
     * Uses given dictionary of words to initialize a new anagram solver.
     *
     * > Assume that the words in the set are in alphabetical order.
     * > Do not modify the set passed to your constructor.
     * > Throw an IllegalArgumentException if the set passed is null.
     *
     * @param dictionary
     */
    public Anagrams(Set<String> dictionary) {
        if (dictionary == null) {
            throw new IllegalArgumentException();
        }
        this.dictionary = dictionary;
        words = new TreeSet<>();
        wordsMap = new HashMap<>();
    }

    /**
     * Returns a set containing all words from dictionary that can be made using
     * some or all of the letters in the given phrase, in alphabetical order.
     *
     * Throws an IllegalArgumentException if the phrase string is null.
     *
     * @param phrase
     * @return
     */
    public Set<String> getWords(String phrase) {
        if (phrase == null) {
            throw new IllegalArgumentException();
        }
        // cache the list so we only build each phrase once
        if (!wordsMap.containsKey(phrase)) {
            wordsMap.put(phrase, buildAnagramList(phrase));
        }
        return wordsMap.get(phrase);
    }

    private Set<String> buildAnagramList(String phrase) {
        LetterInventory li = new LetterInventory(phrase);
        for (String word : dictionary) {
            if (li.contains(word)) {
                words.add(word);
            }
        }
        return words;
    }

    /**
     * Recursively find and print all anagrams that can be formed using all of the
     * letters of the given phrase, in the same order and format reliably. If an
     * empty phrase string is passed, no output generates.
     *
     * Throws an IllegalArgumentException if the phrase string is null.
     *
     * @param phrase
     */
    public void print(String phrase) {
        if (phrase == null) {
            throw new IllegalArgumentException();
        }
        buildAnagramList(phrase);
        LetterInventory li = new LetterInventory(phrase);
        Queue<String> q = new LinkedList<String>();
        displayAnagrams(li, q, 0);
    }

    /**
     * Same as print(String phrase) except output is limited by maximum total number of words.
     *
     * > If max is 0, print all anagrams.
     * > Throw IllegalArgumentException if phrase string is null or max < 0.
     *
     * @param phrase
     * @param max
     */
    public void print(String phrase, int max) {
        if (phrase == null || max < 0) {
            throw new IllegalArgumentException();
        }
        buildAnagramList(phrase);
        LetterInventory li = new LetterInventory(phrase);
        Queue<String> words = new LinkedList<String>();
        displayAnagrams(li, words, max);
    }

    /**
     * Recursively display anagrams
     *
     * @param li
     * @param q
     * @param max
     */
    private void displayAnagrams(LetterInventory li, Queue<String> q, int max) {
        if (li != null && (q.size() <= max || max == 0)) {
            if (li.isEmpty() && !q.isEmpty()) {
                System.out.println(q);
            } else {
                for (String x : words) {
//                    LetterInventory liKey = new LetterInventory(x);

                    for (String word : words) {

                        if (li.contains(word) && !q.contains(word)) {
                            li.subtract(word);
//                        for (String s : words) {
                            q.add(word);
                            displayAnagrams(li, q, max);
                            q.remove(word);
                        }
                    }
                }
            }
        }
    }
}
