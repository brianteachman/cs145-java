import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class WordSearch {

    private final int MAX_TRY = 100;
    private String[][] wordsearch;
    private int longestWord;
    private int rowLength;
    private Scanner input;
    private boolean isGenerated;

    public WordSearch() {
        longestWord = 0;
        rowLength = 0;
        isGenerated = false;
        input = new Scanner(System.in);
    }

    public String menuSelect() {

        printIntro();
        String selection = input.next();
        return selection.substring(0, 1).toLowerCase();
    }

    public void printIntro() {
        System.out.println("Welcome to my word search generator!");
        System.out.println("This programs will allow you to generate your own word search puzzle");
        System.out.println("Please select and option:");
        System.out.println("Generate a new word search (g)");
        System.out.println("Print out your word search (p)");
        System.out.println("Show the solution to your word search (s)");
        System.out.println("Quit the program (q)");
    }

    /**
     * (g) Generate the word search from words collected by the user.
     */
    public void generate() {

//        System.out.println();
        System.out.println("Do you want to load a text file of line delimited words? (y/n)");
//        String choice = input.next();
        String choice = "y";

        // build word list
        String[] words = null;
        if (choice.substring(0, 1).toLowerCase().equals("y")) {
//            System.out.println("The words list file must be in this directory"
//                                + "and each word must be on a new line.");
//            System.out.println("What is the name of your words list file?");
//            String filename = input.next();
//            words = getWordsFromFile(filename);
            words = getWordsFromFile("words.txt");
        } else {
            words = wordListPrompt();
        }

        // use words to generate word search (only try each word MAX_TRY times)
        setupGrid(words);
    }

    private String[] getWordsFromFile(String filename) {
        ArrayList<String> words = new ArrayList<String>();
        try {
            File file = new File( "src/" + filename);
            Scanner fin = new Scanner(file);
            while (fin.hasNext()) {
                String nextWord = fin.next();
                checkLength(nextWord);
                words.add(nextWord);
            }
            fin.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // <T> T[] 	ArrayList.toArray(new T[size])
        return words.toArray(new String[words.size()]);
    }

    private String[] wordListPrompt() {

        System.out.println();
        System.out.println("How many words do you want to enter for your word search?");
        int wordCount = input.nextInt();

        // prompt for each word, one at a time
        String[] words = new String[wordCount];

        System.out.println("Enter all "+wordCount+" words, on a new line each:");
        for (int i = 0; i < wordCount; i++) {
            words[i] = input.next();
            checkLength(words[i]);
        }
        return words;
    }

    private void checkLength(String word) {
        if (word.length() > longestWord) {
            longestWord =  word.length();
        }
    }

    private void setupGrid(String[] words) {
        boolean isBuilt = false;
        rowLength = (longestWord % 2 == 0) ? longestWord + 2 : longestWord + 1;
        wordsearch = new String[rowLength][rowLength];
        int currentWord = 0; // wordIsUsed[currentWord]

        // setup array to keep track of whether word is used
        Boolean[] wordIsUsed = new Boolean[words.length];
        for (int h = 0; h < wordIsUsed.length; h++) {
            wordIsUsed[h] = false;
        }

        while (!isBuilt) {
            for (int h = 0; h < rowLength; h++) {
                for (int i = 0; i < rowLength; i++) {

                    if (currentWord == 0) {
                        setRow(h + 1, words[currentWord]);
//                        setElement(h + 1, i, words[currentWord]);
                    }

                    if (i == words[currentWord].length()) {
//                        currentWord++;
                        isBuilt = true;
                    }

//                    if (currentWord == words.length - 1 && wordIsUsed[currentWord]) {
                    if (currentWord == 1) {
                        isBuilt = true;
                    }
                }
            }
        }

    }

    private void setRow(int row, String word) {

        for (int col = 0; col < rowLength; col++) {
            if (!word.isEmpty() && col < word.length()) {

                setElement(row, col, word);
            }
        }
    }

    private void setElement(int row, int col, String word) {
        if (!word.isEmpty() && col < word.length()) {
            wordsearch[row][col] = ((col + 1) < word.length()) ?
                    word.substring(col, col + 1) :
                    word.substring(col);
        } else {
            wordsearch[row][col] = "-";
        }
    }

    /**
     * (p) Prints out generated word search.
     */
    public void print() {
        printWordSearch();
    }

    /**
     * (s) Prints out solution to generated word search with all
     *     non word letters blanked (-) out.
     */
    public void showSolution() {
        printWordSearch(true);
    }

    private void printWordSearch() {
        printWordSearch(false);
    }

    private void printWordSearch(boolean isSolution) {
        System.out.println();
        for (int row = 0; row < rowLength; row++) {
            for (String character : wordsearch[row]) {
                if (character == "-" && !isSolution) {
                    System.out.print(getRandomLetter() + " ");
                } else {
                    System.out.print(character + " ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    private String getRandomLetter() {
        Random gen = new Random();
        String[] letters = {
                "a", "b", "c", "d", "e",
                "f", "g", "h", "i", "j",
                "k", "l", "m", "n", "o",
                "p", "q", "r", "s", "t",
                "u", "v", "w", "x", "y",
                "z"
        };
        return letters[gen.nextInt(26)];
    }
}
