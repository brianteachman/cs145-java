import java.util.*;

/** 
 * @author Brian Teachman
 * @date 11/19/2018
 */
public class AssassinManager {

    private AssassinNode frontKillring;
    private AssassinNode frontGraveyard;

    /**
     * Create a KillRing of linked nodes from given list of names
     *
     * @param names  List of names in this list of dead men
     */
    public AssassinManager(List<String> names) {
        if (names == null || names.isEmpty()) {
            throw new IllegalArgumentException();
        }
        AssassinNode current = null;
        for (String name : names) {
            if (frontKillring == null) {
                current = frontKillring = new AssassinNode(name);
            } else {
                current.next = new AssassinNode(name);
                current = current.next;
            }
        }
    }

    /**
     * Prints the Killring: "killer is stalking target"
     */
    public void printKillRing() {
        AssassinNode current = frontKillring;
        while(current.next != null) {
            System.out.println("  " + current.name + " is stalking " + current.next.name);
            current = current.next;
        }
        if (!isGameOver()) {
            System.out.println("  " + current.name + " is stalking " + frontKillring.name);
        }
    }

    /**
     * Print names in opposite order which they were killed
     *
     * Most recently killed first, then next more recently killed, and so on
     */
    public void printGraveyard() {
        AssassinNode current = frontGraveyard;
        while(current != null) {
            System.out.println("  " + current.name + " was killed by " + current.killer);
            current = current.next;
        }
    }

    /**
     * Check if a person is in the Killring
     *
     * @param name  The name of the person
     * @return      True if found in list, false otherwise
     */
    public boolean killRingContains(String name) {
        return linkListContains(name, frontKillring);
    }

    /**
     * Check if a person is in the Graveyard
     *
     * @param name  The name of the person
     * @return      True if found in list, false otherwise
     */
    public boolean graveyardContains(String name) {
        return linkListContains(name, frontGraveyard);
    }

    /**
     * Private method that actually iterates through list in search of name
     *
     * @param name      The name of the person
     * @param current   The list to search for name in
     * @return          True if found in list, false otherwise
     */
    private boolean linkListContains(String name, AssassinNode current) {
        while(current != null) {
            if (name.equals(current.name)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    /**
     * @return boolean True if game is over
     */
    public boolean isGameOver() {
        return frontKillring.next == null;
    }

    /**
     * Return the name of the winner of the game, or null if the game is not over.
     *
     * @return
     */
    public String winner() {
        if (isGameOver()) {
            return frontKillring.name;
        }
        return null;
    }

    /**
     * Send target to the Graveyard
     *
     * @param name The name of the AssassinNode to kill
     */
    public void kill(String name) {
        checkGameState();

        // Find targets killer, ignore case in comparing names
        AssassinNode killer = getAssassinNode(name);

        // Assign target from killer
        AssassinNode target = getTargetNode(name, killer);

        // Record who killed the target
        target.killer = killer.name;

        // Add person to graveyard
        target.next = frontGraveyard;   // point to rest of graveyard, may be null
        frontGraveyard = target;        // set to front of graveyard list
    }

    /**
     * Returns the target node from the KillRing
     *
     * @param name      The name of the person
     * @param killer    The AssassinNode of the killer
     * @return          The AssassinNode of the target
     */
    private AssassinNode getTargetNode(String name, AssassinNode killer) {
        AssassinNode target;
        if(name.toLowerCase().equals(frontKillring.name.toLowerCase())) {
            target = frontKillring;      // Target is the first person in the list
            frontKillring = target.next; // Remove from kill ring
        } else {
            target = killer.next;        // The target is the person after the killer
            killer.next = target.next;   // Remove from kill ring
        }
        return target;
    }

    /**
     * Returns the killer node from the KillRing
     *
     * @param name  The name of the assassins target
     * @return      The AssassinNode of the killer
     */
    private AssassinNode getAssassinNode(String name) {
        AssassinNode killer = frontKillring;
        while(killer.next.next != null && !killer.next.name.toLowerCase().equals(name.toLowerCase())) {
            killer = killer.next;
        }

        // If target is first person in the list, set killer to last person
        boolean isTargetFirstNode = name.toLowerCase().equals(frontKillring.name.toLowerCase());
        if (isTargetFirstNode) {
            while(killer.next != null) { // Last person in list is first persons killer
                killer = killer.next;
            }   // This completes one iteration through the list
        }

        return killer;
    }

    /**
     * Checks the game state and throws exception if needed
     */
    private void checkGameState() {
        if (isGameOver()) {
            throw new IllegalStateException();
        }
    }
}
