import java.util.*;

/**
 * This class does something
 */
public class HangmanManager {

    private int wordLength;
    private int guessesLeft;
    private SortedSet<Character> guessList;
    private Collection<String> wordList;
    private Map<String, Set<String>> patterns;
    private String currentPattern;


    /**
     * A dictionary of words, word length, and max no of wrong guesses is passed.
     *
     * @param dictionary The list of words for HangmanManager to select from
     * @param length     The size of the word player wants to guess
     * @param max        The number of guessesLeft
     */
    public HangmanManager(Collection<String> dictionary, int length, int max) {
        if (length < 1) {
            String text = "length of word must be greater than 0.Length put" + length;
            throw new IllegalArgumentException(text);
        }
        if (max < 0) {
            String text = "Max wrong guesses allowed must be at least 0."
                    + " Got max of " + max + "guesses.";
            throw new IllegalArgumentException(text);
        }
        guessesLeft = max;
        wordLength = length;
        wordList = new TreeSet<>();
        guessList = new TreeSet<>();
        patterns = new TreeMap<>();
        currentPattern = initPattern();

        filterWordsByLength(dictionary, length);
    }

    /**
     * Filters words of 'length' from dictionary to set the wordList Set.
     *
     * @param dictionary Collection containing the starting set of words to pull from
     * @param length     The length of the word to guess
     */
    private void filterWordsByLength(Collection<String> dictionary, int length) {
        for (String word : dictionary) {
            if (word.length() == length) {
                wordList.add(word);
            }
        }
    }

    /**
     * Gets access to the wordList set of words being considered.
     *
     * @return The list of words that might be the answer
     */
    public Collection<String> words() {
        return wordList;
    }

    /**
     * Finds out how many guessesLeft the player has left.
     *
     * @return The number of guessesLeft left
     */
    public int guessesLeft() {
        return guessesLeft;
    }

    /**
     * Returns the wordList set of letters that has been guessed.
     *
     * @return guessList
     */
    public SortedSet<Character> guesses() {
        return guessList;
    }

    /**
     * Return the current pattern to be displayed for the hangman game.
     *
     *    - Taking into account guessesLeft that have been made.
     *    - Letters that have not been guessed are displayed as "-".
     *
     * @return pattern The patterns of the game
     */
    public String pattern() {
        if (wordList.isEmpty()){
            throw new IllegalStateException();
        }
        return currentPattern; // current pattern is set in record method
    }

    /**
     * Update the state of the game each guess.
     *
     * @param guess The letter bieng guessed this round
     * @return      Number of occurrences of guessed letter in the new pattern
     */
    public int record(char guess) {
        checkGameState(guess);

        // Record the next guess made by player.
        guessList.add(guess);

        patterns.clear(); // start with empty map

        // foreach word in list, generate pattern and add to map
        for (String word : wordList) {
            addPatternToMap(generatePattern(guess, word), word);
        }

        // Choose pattern with the largest sized list to return
        setCurrentPattern();

        // Appropriately update the number of guessesLeft left.
        if (!pattern().contains(String.valueOf(guess))) {
            guessesLeft--;
        }

        // Decide what set of words to use.
        if (!patterns.isEmpty()) {
            wordList = patterns.get(pattern());
        }

        // Return number of occurrences of guessed letter in the new pattern.
        return numOccurrencesInPattern(guess);
    }

    /**
     * Updates the pattern that we are currently using
     */
    private void setCurrentPattern() {
        int max = 0;
        for (String key : patterns.keySet()) {          // loop through each pattern
            int listSize = patterns.get(key).size();    // length of set at pattern
            if (listSize > max) {                       // if largest list
                max = listSize;                         // update max list size
                currentPattern = key;                   // use this pattern
            }
        }
    }

    /**
     * Insert word to the set found at key having thePattern, create if not found
     *
     * @param thePattern    The pattern associated with word, used as map key
     * @param word          The word to add to associated keys Set value
     */
    private void addPatternToMap(String thePattern, String word) {
        if (patterns.containsKey(thePattern)) {
            patterns.get(thePattern).add(word);
        } else {
            Set<String> patternList = new TreeSet<>();
            patternList.add(word);
            patterns.put(thePattern, patternList);
        }
    }

    /**
     * Create pattern string containing correct guessesLeft in correct location
     *
     * @param guess The letter guessed
     * @param word  The word to compare each letter with
     * @return      The generated string pattern (ie, "--e-")
     */
    private String generatePattern(char guess, String word) {
        String thePattern = currentPattern;
        for (int i = 0; i < wordLength; i++) {
            if (guess == word.charAt(i)) {
                thePattern = addToPattern(guess, thePattern, i);
            }
        }
        return thePattern;
    }

    /**
     * Create a new empty pattern ("----"), the length of the word
     *
     * @return The blank string pattern
     */
    private String initPattern() {
        String init = "";
        for (int i = 0; i < wordLength; i++) {
            init = init + "-";
        }
        return init;
    }

    /**
     * Add letter to pattern string at given location
     *
     * @param guess     The letter to add to pattern
     * @param pattern   The pattern to add the letter to
     * @param index     The index of the pattern string to add letter to
     * @return
     */
    private String addToPattern(char guess, String pattern, int index) {
        if (index == 0) {
            pattern = String.valueOf(guess) + pattern.substring(1);
        } else {
            pattern = pattern.substring(0, index)
                    + String.valueOf(guess)
                    + pattern.substring(index + 1);
        }
        return pattern;
    }

    /**
     * Checks for a non empty word list, guesses left and if you already guessed letter
     *
     * @param guess
     */
    private void checkGameState(char guess) {
        if (guessesLeft() < 1 || wordList.isEmpty()) {
            throw new IllegalStateException();
        }
        if (guessList.contains(guess)) {
            throw new IllegalArgumentException("Char " + guess + " was already guessed");
        }
    }

    /**
     * Returns number times guessed letter found in pattern
     *
     * @param guess The letter guessed by player
     * @return      The count found
     */
    private int numOccurrencesInPattern(char guess) {
        int charCountInWord = 0;
        for (int i = 0; i < pattern().length(); i++) { // loop through each letter
            // if character found at this location
            if (String.valueOf(guess).equals(pattern().substring(i, i+1))) {
                charCountInWord++; // increment # of times letter was found
            }
        }
        return charCountInWord;
    }
}
