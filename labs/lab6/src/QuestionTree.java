import java.io.PrintStream;
import java.util.Scanner;

public class QuestionTree {

    /**
     * Represents both a question and the object
     */
    public class QuestionNode {

        public String data;
        public QuestionNode left;
        public QuestionNode right;
        public QuestionNode parent;

        public QuestionNode(String data, QuestionNode parent, QuestionNode left, QuestionNode right) {
            this.data = data;
            this.parent = parent;
            this.left = left;
            this.right = right;
        }

        // constructor for all child nodes
        public QuestionNode(String data, QuestionNode parent) {
            this(data, parent, null, null);
        }

        // overallRoot constructor
        public QuestionNode(String data) {
            this(data, null, null, null);
        }
    }

    private QuestionNode overallRoot;
    private UserInterface ui;
    private int totalGames;
    private int gamesWon;

    /**
     * Constructor builds tree of questions to ask.
     *
     * @param ui
     */
    public QuestionTree(UserInterface ui) {
        if(ui == null){
            throw new IllegalArgumentException();
        }
        this.ui = ui;
        overallRoot = new QuestionNode("computer");
        totalGames = 0;
        gamesWon = 0;
    }

    /**
     * Plays one complete guessing game with the user, asking yes/no questions until reaching an
     * answer object to guess.
     */
    public void play() {
        QuestionNode object = play(overallRoot);

        System.out.print("Would your object happen to be " + object.data + "? ");
        if (isInputYes()) {
            gamesWon++; // That,s it, computer won.
            // If the computer wins the game, print a message saying so.
            System.out.println("I win!");
        } else {
            // Your tree must ask the user what object he/she was thinking of
            System.out.print("I lose. What is your object? ");
            String newObject = ui.nextLine(); // cat

            // A question to distinguish that object from the player's guess
            System.out.print("Type a yes/no question to distinguish your item from " + object.data + ": ");
            String newQuestion = ui.nextLine(); // Does it meow?

            // Whether the player's object is the yes or no answer for that question
            System.out.print("And what is the answer for your object? "); // yes
            Boolean isQuestionAnswer = isInputYes();

            //TODO: add new data to tree
            addToTree(object, newObject, newQuestion, isQuestionAnswer);

//            System.out.println(overallRoot.data);
        }
        totalGames++;
    }

    /**
     * A game begins with the root node of the tree and ends upon reaching an answer leaf node.
     *
     * @param root  The root node of the tree
     * @return      The the answer leaf node (the object)
     */
    private QuestionNode play(QuestionNode root){

        if(root.left != null && root.right != null) {

            // Is it an animal? yes
            // Can it fly? no
            // Does it have a tail? yes

            System.out.println(root.data);
            if(isInputYes()) {
                root.left = play(root.left);
            } else {
                root.right = play(root.right);
            }
        }
        return root;
    }

    private void addToTree(QuestionNode node, String item, String question, Boolean isQuestionYes) {

        QuestionNode questionNode = new QuestionNode(question);
        questionNode.parent = node.parent;

        if (isQuestionYes) {

            // add new question node the right (yes) node
            questionNode.left = new QuestionNode(item);

            // add previous guess to right (no) node of new question
            questionNode.right = node;
        } else {
            questionNode.left = node;
            questionNode.right = new QuestionNode(item);
        }

        if (questionNode.parent == null) {
            overallRoot = questionNode;
        }
        node.parent = questionNode;

        System.out.println(questionNode.data);
        System.out.println(questionNode.left.data);
        System.out.println(questionNode.right.data);
        System.out.println(questionNode.parent);

    }

    private boolean isInputYes() {
        return ui.nextLine().substring(0, 1).toLowerCase().equals("y");
    }

    /**
     * Stores the current tree state to an output file represented by the given PrintStream.
     *
     * Write nodes in the order produced by a preorder traversal of the tree.
     *
     * @param output
     */
    public void save(PrintStream output){
        save(output,overallRoot);
    }

    //Store current tree state to an output file, output.
    //Grows the question tree each time the user runs the program
    //A tree is specified by a sequence of lines, one for each node
    private void save(PrintStream output, QuestionNode root) {
        if(output == null || root == null) {
            throw new IllegalArgumentException();
        }
        if(root.left != null && root.right != null) {

            System.out.println("Q: " + root.data);

            output.println("Q: " + root.data);
            save(output, root.left);
            save(output, root.right);
        } else {
            System.out.println("A: " + root.data);
            output.println("A: " + root.data);
        }
    }

    /**
     * Replace the current tree by reading another tree from a file
     *
     * @param input
     */
    public void load(Scanner input){
        if (input == null) {
            throw new IllegalArgumentException();
        }
//        overallRoot = load(input, overallRoot);
        load(input, overallRoot);
    }

    //Replace current tree by reading another tree from a file
    //Param: Scanner input that reas from a file
    //Replace current tree node with new tree using the information in the file
    private QuestionNode load(Scanner input, QuestionNode root){

//        if(input.hasNextLine() && root != null) {
          if(input.hasNextLine()) {
            String[] parts = input.nextLine().split(":");

            if(parts[0].equals("Q")) {
                root.left = new QuestionNode(parts[1], root);
                System.out.println(parts[1]);

                root.right = load(input, root.right);
            } else if (parts[0].equals("A")) {
                root.left = load(input, root.left);
                root.right = new QuestionNode(parts[1], root);
                System.out.println(parts[1]);

            }

        }
        return root;
    }

    private void insert(String data, QuestionNode root) {

    }

    public int totalGames() {
        return totalGames;
    }

    public int gamesWon() {
        return gamesWon;
    }
}
