import java.util.*;

public class GrammarSolver {

    SortedMap<String, String[]> grammars = new TreeMap<>();

    /**
     * Stores a list of grammars into a SortedMap.
     *
     * @param grammar
     */
    public GrammarSolver(List<String> grammar) {
        for (String s : grammar) {
            String[] parts = s.split(":");
            if (grammars.containsKey(parts[0])) {
               throw new IllegalArgumentException();
            }
            String[] terminal = parts[1].split("[|]");
            grammars.put(parts[0], terminal);
        }
//         System.out.println(grammars);
    }

    /**
     * Returns true if the given symbol is a non-terminal of the grammar; returns false otherwise.
     *
     * @param symbol
     * @return
     */
    public boolean grammarContains(String symbol) {
        return grammars.containsKey(symbol);
    }

    /**
     * Randomly generate the given number of occurrences of the given symbol
     * and return the result as an array of Strings.
     *
     * @param symbol
     * @param times
     * @return
     */
    public String[] generate(String symbol, int times) {
        if (grammars.containsValue(symbol)) {
            throw new IllegalArgumentException();
        }

        String[] sentence = new String[times];
        for (int i = 0; i < times; i++) {
            if (grammars.containsKey(symbol)) {
               
            }
        }
        return sentence;
    }
    
    private String generate(String symbol) {
        return "";
    }

    /**
     * Return a String representation of the various non-terminal symbols
     * from the grammar as a sorted, comma-separated list enclosed in
     * square brackets, as in "[<np>, <s>, <vp>]"
     *
     * @return
     */
    public String getSymbols() {
        return grammars.keySet().toString();
    }
}
