/**
 * This program will allow the user to play a number
 * guessing game one or multiple times.
 * After the user guesses the correct value, they may play
 * again if they had chosen to do so.  The number is
 * limited to values between 1-100.
 *
 * Authors: Carter, Amber, Brian
 */

import java.util.Random;
import java.util.Scanner;

public class Guess {

    private static final int MAX = 100;
    private static int guessCount = 0;
    private static int totalGuesses = 0;
    private static int gameCount = 0;
    private static int bestGame = 0;
    private static Scanner input;
    private static Random randGenerator;

    public static void main(String[] args) {

        input = new Scanner(System.in);
        randGenerator = new Random(MAX);
        boolean gameIsRunning = true;

        gameIntro();
        while (gameIsRunning) {
            guessCount = 0;
            if (runGame()) { // runGame is true at the end of each game
                System.out.print("Do you want to play again? ");
                String replay = input.next().substring(0, 1).toLowerCase();
                if (replay.equals("n")) gameIsRunning = false;
            }
        }
        showResults();
    }

    /**
     * Prints what the program does for the user
     */
    public static void gameIntro() {

        System.out.println("This program allows you to play a guessing game.");
        System.out.println("I will think of a number between 1 and");
        System.out.println("100 and will allow you to guess until");
        System.out.println("you get it. For each guess, I will tell you");
        System.out.println("whether the right answer is higher or lower");
        System.out.println("than your guess.");

    }

    /**
     * Run the game for the user one time.
     *
     * @return boolean
     */
    public static boolean runGame() {

        // generates a value between 0 - 100
        int theAnswer = pickNumber();

        System.out.println();
        System.out.print("I'm thinking of a number between 1 and "+MAX+"... \nYour guess?");

        boolean guessed = false;
        while (!guessed) {
            int choice = Integer.parseInt(input.next());
            // assumes that out of bound values are acceptable

            guessCount++;  // starts at 1, then counts each guess

            // checks if parameter int guess is the value, hint higher or lower
            if (choice < theAnswer) {

                System.out.print("It's higher.");

            } else if (choice > theAnswer) {

                System.out.print("It's lower.");

            } else {

                String pluralize = (guessCount > 1)? "es": "";
                System.out.println("You got it right in "+ guessCount + " guess" + pluralize);
                guessed = true;

            }
            totalGuesses++; // keep track of total guesses
        }
        if (bestGame == 0 || guessCount < bestGame) {
            bestGame = guessCount;
        }
        gameCount++;

        return guessed; // returns boolean
    }

    /**
     * Game statistics/results of all played games
     */
    public static void showResults() {
        double gameStat = totalGuesses / gameCount;
        System.out.println();
        System.out.println("Overall results:");
        System.out.println("\ttotal games   = " + gameCount);
        System.out.println("\ttotal guesses = " + totalGuesses);
        System.out.println("\tguesses/game  = " + gameStat);
        System.out.println("\tbest game     = " + bestGame);
    }

    /**
     * Generate and return a random number using a Random object.
     *
     * @return int  The random number
     */
    private static int pickNumber() {
        return randGenerator.nextInt(MAX) + 1;
    }

}