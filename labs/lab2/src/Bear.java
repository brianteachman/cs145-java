import java.awt.*;

public class Bear extends Critter {

    private boolean isPolarBear;
    private int moveCount;

    public Bear(boolean polar) {

        isPolarBear = polar;
        moveCount = 0;

    }

    /**
     *
     */
    public Action getMove(CritterInfo info) {

        moveCount++;

        Action nextMove = null;

        // always infect if an enemy is in front
        if (info.getFront() == Neighbor.OTHER) {
            nextMove = Action.INFECT;
        }
        // otherwise turn left
        else if (info.getFront() == Neighbor.WALL) {
            nextMove = Action.LEFT;
        }
        // otherwise hop if possible
        else if (info.getFront() == Neighbor.EMPTY) {
            nextMove = Action.HOP;
        }
        else { // what if hop is not possible?
            //TODO: not sure if on collision, wait for one cycle, or turn left?
            nextMove = Action.LEFT;
        }

        return nextMove;
    }

    public Color getColor() {

        return isPolarBear ? Color.WHITE: Color.BLACK;

    }

    public String toString() {

        return (moveCount % 2 == 0) ? "/": "\\";

    }

}