import java.awt.*;

public class Giant extends Critter {

    private String[] giantSpeech  = {"fee", "fie", "foe", "fum"};
    private int wordIndex;
    private int moveCount;

    public Giant() {

        moveCount = 0;
        wordIndex = 0;

    }

    /**
     *
     * @param info
     * @return Action
     */
    public Action getMove(CritterInfo info) {

        moveCounter();

        // always infect if an enemy is in front
        if (info.getFront() == Neighbor.OTHER) {
            return Action.INFECT;
        }
        // otherwise hop if possible
        else if (info.getFront() != Neighbor.SAME && info.getFront() != Neighbor.WALL) {
            return Action.HOP;
        }
        // otherwise turn right
        else {
            return Action.RIGHT;
        }

    }

    /**
     * Randomly picks one of three colors (Color.RED, Color.GREEN, Color.BLUE)
     * and uses that color for three moves, then randomly pick again.
     *
     * @return Color  The color of the text representation of the lion
     */
    public Color getColor() {

        return Color.GRAY;

    }

    /**
     * Cycles through "fee", "fie", "foe", "fum" every six moves
     *
     * See moveCounter()
     *
     * @return String  The text output
     */
    public String toString() {

        return giantSpeech[wordIndex];

    }

    /**
     * Checks for every six moves, then increment moveCount,
     * if true update the array index.
     */
    private void moveCounter() {

        if (moveCount++ % 6 == 0) {
            wordIndex = (wordIndex < 3)? wordIndex + 1: 0;
        }

    }

}