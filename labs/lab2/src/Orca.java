/**
 * Orca model for Lab 2 CS 145 class
 *
 * @author  Brian Teachman
 */

import java.awt.*;

public class Orca extends Critter {

    private int moveCount;
    private boolean isRight;
    private boolean changeColor;
    private boolean isHoming;

    public Orca() {

        moveCount = 0;
        isRight = false;
        changeColor = false;
        isHoming = false;

    }

    /**
     * Orca move in pods
     *
     * @param info
     * @return Action
     */
    public Action getMove(CritterInfo info) {

        moveCounter();

        if (isHoming) { // currently isHoming only false
            return homingMove(info);
        } else {
            return defaultMove(info);
        }
    }

    /**
     * If Orca is not homing in on the pod, do this
     *
     * @param info
     * @return Action  The move to perform
     */
    private Action defaultMove(CritterInfo info) {
        if (info.getFront() == Neighbor.OTHER) {
            return Action.INFECT;
        }
        else if (info.getFront() != Neighbor.EMPTY ) {
            // Remember, change is good :)
            return isRight ? Action.RIGHT: Action.LEFT;
        }
        else {
            return Action.HOP;
        }
    }

    /**
     * @return Color  The color of the text representation of the orca
     */
    public Color getColor() {

        return changeColor? Color.RED: Color.MAGENTA;

    }

    /**
     * Tie-Fighter
     *
     * @return String  The text output
     */
    public String toString() {

        return "|o|";

    }

    private void moveCounter() {

        moveCount++;
        if (moveCount % 3 == 0) isRight = !isRight;
        if (moveCount % 4 == 0) changeColor = !changeColor;

    }

    /**
     * Based off Nathans idea
     *
     * @param info
     * @return Action  Move this Orca towards the pod
     */
    private Action homingMove(CritterInfo info) {

        // Infect if you can
        if (info.getFront() == Neighbor.OTHER) {
            return Action.INFECT;
        }

        if (info.getFront() == Neighbor.SAME) {
            return isRight ? Action.RIGHT: Action.LEFT;
        }

        // Top right corner
        if (info.getDirection() == Direction.NORTH
            && info.getFront() == Neighbor.WALL
            && info.getRight() == Neighbor.WALL) {
            // set top-right
        }
        // Top left corner
        else if (info.getDirection() == Direction.NORTH
                 && info.getFront() == Neighbor.WALL
                 && info.getLeft() == Neighbor.WALL) {
            // set top-left
        }

        // Turn right to get to top left
        if (info.getDirection() == Direction.WEST && info.getFront() == Neighbor.WALL) {
            return Action.RIGHT;
        }
        // Turn right to get to bottom left
        else if (info.getDirection() == Direction.SOUTH
                 && info.getFront() == Neighbor.WALL) {
            return Action.RIGHT;
        }

        // Turn left to get to top right
        if (info.getDirection() == Direction.EAST && info.getFront() == Neighbor.WALL) {
            return Action.LEFT;
        }
        // Turn left to get to top left
        else if (info.getDirection() == Direction.NORTH
                && info.getFront() == Neighbor.WALL
                && info.getLeft() != Neighbor.WALL || info.getRight() != Neighbor.WALL) {
            return Action.LEFT;
        }

        return Action.HOP;
    }

}