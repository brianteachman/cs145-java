import java.awt.*;
import java.util.Random;

public class Lion extends Critter {

    private int moveCount;
    private int colorIndex;
    private Random randGenerator;

    public Lion() {

        moveCount = 0;
        colorIndex = 0;
        randGenerator = new Random(300300);

    }

    /**
     *
     * @param info
     * @return Action
     */
    public Action getMove(CritterInfo info) {

        moveCounter();

        // always infect if an enemy is in front
        if (info.getFront() == Neighbor.OTHER) {
            return Action.INFECT;
        }
        // otherwise if a fellow Lion is in front, then turn right
        else if (info.getFront() == Neighbor.SAME && info.getRight() != Neighbor.WALL) {
            return Action.RIGHT;
        }
        // otherwise if a wall is in front or to the right, then turn left
        else if (info.getFront() == Neighbor.WALL || info.getRight() == Neighbor.WALL) {
            return Action.LEFT;
        }
        // otherwise hop
        else {
            return Action.HOP;
        }

    }

    /**
     * Randomly picks one of three colors (Color.RED, Color.GREEN, Color.BLUE)
     * and uses that color for three moves, then randomly pick again.
     *
     * @return Color  The color of the text representation of the lion
     */
    public Color getColor() {

        Color[] color = {Color.RED, Color.GREEN, Color.BLUE};
        return color[colorIndex];

    }

    public String toString() {

        return "L";

    }

    private void moveCounter() {

        if (moveCount++ == 3) {
            moveCount = 0;
            colorIndex = randGenerator.nextInt(3);
        }

    }

}