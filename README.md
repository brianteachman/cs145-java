## CS 145: Computer Programming Fundamentals II

My working dir for CS145 class at Whatcom Communtiy College, based on the book Building Java Programs - A Back to Basics Approach (2E).

This code is product of my own efforts in CS145, Computer Programming Fundamentals II class in the Fall of 2018.

### Topics

1. Searching and sorting
2. Object oriented design
3. Error handling
4. File input/output
5. Event based programming
6. Bitwise operators
7. Multithreaded and network programming

### Links

* [Java 8 API](http://docs.oracle.com/javase/8/docs/api/overview-summary.html)
* [http://www.buildingjavaprograms.com/supplements4.shtml](http://www.buildingjavaprograms.com/supplements4.shtml)
* [https://practiceit.cs.washington.edu/](https://practiceit.cs.washington.edu/)
